import numpy as np
#import lens_params
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import rc
import random
from scipy import constants
import pandas as pd
from datetime import datetime
import json
import bilby
import os
from pycbc import detector
import pycbc
from pycbc import waveform, filter, psd

## inputs go here(only for getting path of the injection parameter file)
z_file = 'z_PDF.dat.txt'
z_pdf_model = 'MadauDickinson'
pop_file = '/home1/apratim.ganguly/projects/lensing/O3_runs/o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_result.json'
N=int(1e7)
n = 2*N #buffer

z_max = 7
seed = 8
lensing_analysis =True
outlabel = 'sim_O3'
odir='injs_corrected_19082022'
#psd_dir = "/home1/srashti.goyal/lensid/data/PSDs/O3a_representative_psd"
psd_dir = "/home1/srashti.goyal/lensid/data/PSDs/O4_projected"


#out = odir+'/'+outlabel+'_N_'+str(N)+'_zmax_' + str(z_max)+ '_'+z_pdf_model+'_seed_'+ str(seed)

### PSDs
asd_file = {}
asd_file["H1"] = (
    "%s/H1.txt" % psd_dir
)
asd_file["L1"] = (
    "%s/L1.txt" % psd_dir
)
asd_file["V1"] = "%s/V1.txt" % psd_dir

waveform_arguments = dict(
    waveform_approximant="IMRPhenomXPHM",
    reference_frequency=20.0,
    minimum_frequency=20.0)
delta_f = 1./64
f_lower = 20.
f_high = 2048.
psd_length = int(f_high/delta_f)

asd_pycbc_dict = {}
asd_pycbc_dict['H1'] = psd.read.from_txt(asd_file['H1'], psd_length, delta_f, f_lower, is_asd_file=True)
asd_pycbc_dict['L1'] = psd.read.from_txt(asd_file['L1'], psd_length, delta_f, f_lower, is_asd_file=True)
asd_pycbc_dict['V1'] = psd.read.from_txt(asd_file['V1'], psd_length, delta_f, f_lower, is_asd_file=True)

o3a_psd = {}
o3a_psd['V1']= psd.analytical.AdvVirgo(psd_length, delta_f, f_lower)
o3a_psd['H1']= psd.analytical.aLIGOZeroDetHighPower(psd_length, delta_f, f_lower)
o3a_psd['L1']= psd.analytical.aLIGOZeroDetHighPower(psd_length, delta_f, f_lower)
### bilby WF generator
waveform_generator = bilby.gw.WaveformGenerator(
        duration=64,
        sampling_frequency=2048,
        frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
        waveform_arguments=waveform_arguments,
    )
freq_array=waveform_generator.frequency_array
H1 = detector.Detector('H1')
L1 = detector.Detector('L1')
V1 = detector.Detector('V1')
msun = 2e30


def inject_ifos_pycbc(injection_parameters,dets=[H1,L1,V1],asd_pycbc_dict=asd_pycbc_dict,freq_array=freq_array):
    mass_1, mass_2, luminosity_distance, a_1, tilt_1, phi_12, a_2, tilt_2, phi_jl, theta_jn, phase,ra,dec,psi,geocent_time=injection_parameters
    fac=(mass_1+mass_2)*constants.gravitational_constant*msun/constants.speed_of_light**3
    if 0.1/fac <f_lower:
        return np.zeros(len(dets)).tolist()
    else:
        pols=bilby.gw.source.lal_binary_black_hole(freq_array,mass_1, mass_2, luminosity_distance, a_1, tilt_1, phi_12, a_2, tilt_2, phi_jl, theta_jn, phase)
        snrs=[]
        for det in dets:
            Fp, Fc = det.antenna_pattern(ra, dec, psi, geocent_time)
            h = Fp*pols['plus'] + Fc*pols['cross']
            h=pycbc.types.frequencyseries.FrequencySeries(h,delta_f)
            snr = filter.matchedfilter.sigma(h, psd=asd_pycbc_dict[det.name],\
                                        low_frequency_cutoff=f_lower, high_frequency_cutoff=f_high)
            snrs.append(snr)
        return snrs

## imp column names for injections
bilby_params=['mass_1', 'mass_2', 'luminosity_distance', 'a_1', \
'tilt_1', 'phi_12', 'a_2', 'tilt_2', 'phi_jl', 'theta_jn', 'phase','ra','dec','psi','geocent_time']


out='samples_bbh'
bbh_df=pd.read_csv(out+'.csv',index_col=0)

from multiprocessing import Pool
pool=Pool(processes=16)
#bbh_df=bbh_df[:1000]  ##if compute for partial set
SNRs=np.array(pool.map(inject_ifos_pycbc, bbh_df[bilby_params].values))
bbh_df['snr H1'],bbh_df['snr L1'],bbh_df['snr V1']=SNRs[:,0],SNRs[:,1],SNRs[:,2]
bbh_df.to_csv(out+'_with_snrs_O4.csv')


"""
bilby_params=['mass_1', 'mass_2', 'luminosity_distance', 'a_1', \
'tilt_1', 'phi_12', 'a_2', 'tilt_2', 'phi_jl', 'theta_jn', 'phase','ra','dec','psi','geocent_time']
SNRs= []
det_list=['H1','L1','V1']
for i in tqdm(range(len(bbh_df))):
   # ps = {k : bbh_df[k][i] for k in bilby_params }
   # SNR=inject_ifos_bilby(ps,ifos)
    SNR=inject_ifos_pycbc(bbh_df[bilby_params].iloc[i])

    SNRs.append(SNR)
"""
