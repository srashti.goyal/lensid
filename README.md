# Strong LENSing IDentification with Machine Learning

This project aims to classify the pair of gravitational wave signals as lensed or unlensed using machine learning, in low latency. The input is Q-transforms and bayestar skymaps which are generated in less than a minute. Methods paper can be found on arXiv [2106.12466](https://arxiv.org/abs/2106.12466)
 
Kindly follow the **[installation instructions](https://git.ligo.org/srashti.goyal/lensid/-/wikis/Installation-instructions)** page for running the pipeline. 

Visit **[code review](https://git.ligo.org/srashti.goyal/lensid/-/wikis/Code-Review)** page for the details of various scripts/notebooks.

If you use the code in this repository kindly [cite](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.104.124057) our methods paper.
