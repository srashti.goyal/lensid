import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import warnings
warnings.filterwarnings('ignore')
import os

#info_df = pd.read_csv('/home/srashti.goyal/lensid_O4/data_download_preparation/O4a_events_data/events_info_df.csv',index_col=0)

def bhattacharya_coeff_formula(mu1,sig1,mu2,sig2,theta=np.linspace(1,1001,10000)):
    bc = np.sqrt(2*sig1*sig2/(sig1**2 + sig2**2))*np.exp(-((mu1-mu2)**2) / (4*(sig1**2 + sig2**2)))
    return bc

bhattacharya_coeff_formula_v=np.vectorize(bhattacharya_coeff_formula)   
def bhattacharya_dist(mu1,sig1,mu2,sig2):
    return 1/4 * np.log(1/4 * ((sig1/sig2)**2 + (sig2/sig1)**2 + 2 ) ) + 1/4 * ((mu1-mu2)**2) / (sig1**2 + sig2**2)
bhattacharya_dist_v=np.vectorize(bhattacharya_dist)


def get_mass_snr(tag_0,tag_1,info_df):
    mchirp0,snr0=info_df[info_df['GRACEDBSID']==tag_0][['ChirpMass','NetworkSNR']].values[0]
    mchirp1,snr1=info_df[info_df['GRACEDBSID']==tag_1][['ChirpMass','NetworkSNR']].values[0]
    return [mchirp0,snr0,mchirp1,snr1]


