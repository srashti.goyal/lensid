import matplotlib.pylab as plt
import numpy as np
import os
import argparse
import glob
import cv2


def main():
    parser = argparse.ArgumentParser(
        description='This is stand alone code for converting .fits skymaps to .npz')

    parser.add_argument(
        '-indir',
        '--indir',
        help='input .png files directory ',
        required=True)
    parser.add_argument(
        '-outdir',
        '--outdir',
        help='out .npz files directory',
        required=True)
    parser.add_argument(
        '-force',
        '--force',
        help='overwrite the output .npz file if exists already..(0/1)',
        default=0)
    parser.add_argument(
        '-n',
        '--n',type=int,
        help='number of files to convert. Default=-1 i.e. all',
        default=-1)

    args = parser.parse_args()
    indir = args.indir
    outdir = args.outdir
    n=args.n
    force=args.force
    path = indir+'*.png'
    if os.path.exists(outdir)==0:
        os.makedirs(outdir)
    path = indir+'*.png'
    files = glob.glob(path)
    print('converting %d files to npz '%len(files))
    v_main=np.vectorize(_main)
    filenames=files[:n]
    out_filenames=[outdir+filename1.split('/')[-1][:-4] + '.npz' for filename1 in filenames]
    v_main(filenames,out_filenames,force=force)
    
def _main(filename1, out_filename1,img_rows=128,img_cols=128,force=1):
    if (os.path.isfile(filename1) == 1):
        # print(out_filename1)
        if (os.path.isfile(out_filename1) != 1) or (force == 1):
                img = cv2.imread(filename1)
                img = cv2.resize(img, (img_rows, img_cols))
                np.savez(out_filename1,data=img)


if __name__ == '__main__':
    main()
