import pylab
import pycbc.noise
import pycbc.psd
from pycbc.waveform import get_td_waveform
from pycbc.detector import Detector
from numpy.core.defchararray import add
import os
import numpy as np
import pandas as pd
import sys
import lensid.utils.qt_utils as qtils
import argparse


def main():
    parser = argparse.ArgumentParser(
        description='This is stand alone code for generating QTs for unlensed injection dataset')

    parser.add_argument(
        '-odir',
        '--odir',
        help='Output directory',
        default='check')
    parser.add_argument(
        '-start',
        '--start',
        type=int,
        help='unlensed inj start index',
        default=0)
    parser.add_argument(
        '-whitened',
        '--whitened',
        help='1/0 default 1',
        type=int,
        default=1)
    parser.add_argument(
        '-n',
        '--n',
        type=int,
        help='no. of unlensed injs',
        default=0)
    parser.add_argument(
        '-mode',
        '--mode',
        type=int,
        help='enter no : 1. default \t 2. test',
        default=1)
    parser.add_argument(
        '-infile',
        '--infile',
        help='.npz unlensed injs file path to load tags from',
        required=1)
    parser.add_argument(
        '-psd_mode',
        '--psd_mode',
        type=int,
        help='enter no : 1. analytical \t 2.load from files',
        default=1)
    parser.add_argument(
        '-single_det',
        '--single_det',
        help='optional, if produce QT for single det, provide det name: H1,L1,V1',
        default=0)
    parser.add_argument(
        '-asd_dir',
        '--asd_dir',
        help='optional, give directory where psd files are in format H1.txt, L1.txt, V1.txt',
        default=None)
    parser.add_argument(
        '-qrange',
        '--qrange',
        type=int,
        help='1. old qrange 3,7 for m1>60 and 4,10 otherwise. 2. default  is 3,30',
        default=2)
    args = parser.parse_args()
    print('\n Arguments used:- \n')

    for arg in vars(args):
        print(arg, ': \t', getattr(args, arg))

    if args.psd_mode == 1:
        psd_mode = 'analytical'
        psd_H, psd_L, psd_V = qtils.inj_psds_HLV(psd_mode=psd_mode)

    elif args.psd_mode == 2:
        psd_mode = 'load'
        psd_H, psd_L, psd_V = qtils.inj_psds_HLV(
            psd_mode=psd_mode, asd_dir=args.asd_dir)
    else:
        print('invalid psd_mode choice')
    duration = 64
    psd_dict={'H1':psd_H,'L1':psd_L,'V1':psd_V}
    if args.single_det ==0:
        dets = ['H1','L1','V1']
    else:
        dets = [args.single_det]    
    data = np.load(args.infile,allow_pickle=True)

    if args.mode == 1:
        lost_ids = []
    elif args.mode == 2:
        lost_ids = np.array([55, 57, 68, 76, 182, 207, 225, 277])
    else:
        print('mode not found')

    ntot = data['mass_1'].shape[0]
    if args.n == 0:
        args.n = ntot

    whitened = args.whitened

    odir = args.odir

    if not os.path.exists(odir):
        os.makedirs(odir)
    try:
        for det in dets:
            os.makedirs(odir + '/'+det)
    except BaseException:
        print(odir, ' already exists. saving Qtransforms to it.')

    for count, i in enumerate(range(args.start, args.start + args.n)):
        if args.qrange == 1:
            if data["mass_1"][i] < 60:
                q = qtils.q_msmall
            else:
                q = qtils.q_mlarge
        else:
            q = qtils.q_wide
        hp, hc = get_td_waveform(approximant="IMRPhenomXPHM", mass1=data["mass_1"][i], mass2=data['mass_2'][i], inclination=data["theta_jn"][i], delta_t=1.0 / 2**12, f_lower=15, f_higher=1000, distance=data["luminosity_distance"][i], coa_phase=data['phase'][i],spin_1x=data['spin_1x'][i],spin_1y=data['spin_1y'][i],spin_1z=data['spin_1z'][i],spin_2x=data['spin_2x'][i],spin_2y=data['spin_2y'][i],spin_2z=data['spin_2z'][i])
        # flower=30 works without whitening
        print(data["mass_1"][i], data['mass_2'][i])

        end_time = data["geocent_time"][i]
        declination = data['dec'][i]
        right_ascension = data["ra"][i]
        polarization = data["psi"][i]
        hp.start_time += end_time
        hc.start_time += end_time
        for det in dets:
            pycbc_det=Detector(det)
            signal = pycbc_det.project_wave(
                hp, hc, right_ascension, declination, polarization)

            noise_signal = qtils.inject_noise_signal(
                signal, psd_dict[det], duration=duration, whitened=whitened)
        
            fname = str(data['event_tag'][i])
            if whitened == 1:
                fname = fname + '-whitened'
            power = qtils.plot_qt_from_ts(
                noise_signal, data["geocent_time"][i], q, outfname=odir + "/"+det+"/" + fname)

            print(i)
