import lensid.utils.ml_utils as ml
import pandas as pd
import warnings
import os
import argparse
import numpy as np
import time
import tensorflow as tf

import matplotlib.pyplot as plt


warnings.filterwarnings('ignore')


def main():
    parser = argparse.ArgumentParser(
        description='This is stand alone code for training Densenets for H or L or V detector using the lensed and unlensed simulated events qtransforms')
    parser.add_argument(
        '-lensed_df',
        '--lensed_df',
        help='input lensed Dataframe path',
        default='/home/srashti.goyal/lensid_runs/uniform_dataset/dataframe/lensed.csv')
    parser.add_argument(
        '-unlensed_df',
        '--unlensed_df',
        help='input unlensed Dataframe path',
        default='/home/srashti.goyal/lensid_runs/uniform_dataset/dataframe/unlensed_half.csv')

    parser.add_argument(
        '-data_dir',
        '--data_dir',
        help='QTs images folder path',
        default='/home/srashti.goyal/lensid_runs/uniform_dataset/npz/')
    parser.add_argument(
        '-colored',
        '--colored',
        type=int,
        help='QTs images to be used  0. grayscale with individual + superposition (default) or 1. colored superimposed. ',
        default=0)
    parser.add_argument(
        '-file_type',
        '--file_type',
        help='QTs images file type npz  or png (default)',
        default='npz')
    parser.add_argument(
        '-det',
        '--det',
        help='detector to test: H1 L1 V1',
        default='H1')
    parser.add_argument(
        '-data_dir_0',
        '--data_dir_0',
        help='QTs images 0 folder path',
        default=None)
    parser.add_argument(
        '-data_dir_1',
        '--data_dir_1',
        help='QTs images 1 folder path',
        default=None)    


    parser.add_argument(
        '-whitened',
        '--whitened',
        type=int,
        help='1/0',
        default=1)


    parser.add_argument(
        '-model_id',
        '--model_id',
        help='model id to include in output DF columns and trained densenet',
        default=0)
 
    parser.add_argument(
        '-det',
        '--det',
        help='which detector(H1 or L1 or V1)',
        default='H1')

    parser.add_argument(
        '-size_lensed',
        '--size_lensed',
        help='no. of lensed events to train on',
        type=int,
        default=1400)
    parser.add_argument(
        '-size_unlensed',
        '--size_unlensed',
        help='no. of unlensed events to train on',
        type=int,
        default=1400)
    parser.add_argument(
        '-epochs',
        '--epochs',
        help='no. of epochs to train for',
        type=int,
        default=20)
    parser.add_argument(
        '-lr',
        '--lr',
        help='initial learing rate for training',
        type=float,
        default=0.01)
    parser.add_argument(
        '-odir',
        '--odir',
        help='output trained densenet models directory. Saved densenet path will be ODIR/DET_MODEL_ID.h5',
        required=1)
    parser.add_argument(
        '-batch_size',
        '--batch_size',
        help='Batch size for mini-batch training. Deafult 1000',
        type=int,
        default=1000)

    args = parser.parse_args()
    print('\n Arguments used:- \n')

    for arg in vars(args):
        print(arg, ': \t', getattr(args, arg))


    data_dir = args.data_dir
    dense_model = args.dense_model
    model_id =  args.model_id
    whitened = args.whitened
    data_dir_0 = args.data_dir_0
    data_dir_1 = args.data_dir_1
    file_type=args.file_type
    colored = args.colored
    det = args.det
    data_dir = args.data_dir
    odir = args.odir
    lensed_df = args.lensed_df
    unlensed_df = args.unlensed_df
    size_lensed =args.size_lensed
    size_unlensed =args.size_unlensed
    epochs,lr = args.epochs, args.lr
    batch_size=args.batch_size
    _main(odir, data_dir, lensed_df, unlensed_df, size_lensed, size_unlensed, batch_size,det, epochs, lr, whitened, file_type, colored, model_id, data_dir_0, data_dir_1)
def _main(odir, data_dir, lensed_df, unlensed_df, size_lensed, size_unlensed, batch_size,det, epochs, lr, whitened, file_type, colored, model_id, data_dir_0=None, data_dir_1=None):

    if not os.path.exists(odir):
        os.makedirs(odir)
    df_lensed = pd.read_csv(lensed_df)
    df_lensed = df_lensed.drop(columns=['Unnamed: 0'])
    df_lensed['img_0'] = df_lensed['img_0'].values
    df_lensed['img_1'] = df_lensed['img_1'].values
    df_lensed = df_lensed[:size_lensed]
    df_unlensed = pd.read_csv(unlensed_df)
    df_unlensed = df_unlensed.drop(columns=['Unnamed: 0'])
    df_unlensed = df_unlensed.sample(
        frac=1, random_state=42).reset_index(
        drop=True)[
            :size_unlensed]
    df_train = pd.concat([df_lensed, df_unlensed], ignore_index=True)
    df_train = df_train.sample(frac=1).reset_index(drop=True)
    FMDictTrain=dict(det=det,
        data_mode_dense="current",
        whitened=whitened,
        data_dir=data_dir,
        data_dir_0=data_dir_0,
        data_dir_1=data_dir_1,        
        file_type = file_type,
        colored=colored,
        img_0_paths='default',
        img_1_paths='default')
        
    start_time = time.time()


    if 3000 >=  len(df_train): 
        X, y, missing_ids, df_train = ml.generate_resize_densenet_fm(df_train).DenseNet_input_matrix(
            **FMDictTrain)

        model,history = ml.train_densenet(
            X, y, det, epochs, lr)  # 20,0.01, .005
    else:
        lr_schedule = tf.keras.callbacks.LearningRateScheduler(ml.lrfn, verbose=1)
        size_train=int(len(df_train)*0.8)

        _,model=ml.DensenetModel(lr)



        training_generator = ml.DataGeneratorqts(df_train[:size_train], df_train[:size_train].Lensing.values, batch_size=batch_size,**FMDictTrain)
        validation_generator = ml.DataGeneratorqts(df_train[size_train:], df_train[size_train:].Lensing.values,batch_size=batch_size,**FMDictTrain)
        history = model.fit_generator(generator=training_generator\
                            ,validation_data=validation_generator,callbacks=lr_schedule,epochs=epochs)
    plt.figure()
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(odir+'accuracy_epochs.png')
    plt.figure()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(odir+'loss_epochs.png')
    
    end_time = time.time()
    print("Time Taken: ", end_time - start_time)

    outfile =odir + det +'_'+str(model_id)+ '.h5'
    model.trainable=True


    model.save(outfile)
    print('trained model saved as: ', outfile)


if __name__ == "__main__":
    main()
