import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
os.environ['OMP_NUM_THREADS'] = '4'
os.environ['OPENBLAS_NUM_THREADS'] = '1'
print('\n\n ok \n\n')
import yaml
import matplotlib
import numpy as np
import matplotlib.pylab as plt
import pandas as pd
import argparse
import lensid.utils.ml_utils as ml
import lensid.feature_extraction.lensid_get_features_qts_ml as qts_ml_features
import lensid.feature_extraction.lensid_get_features_sky_ml as sky_ml_features
from pathlib import Path
import json
import lensid.utils.bc_utils as bc

import pycbc.conversions as conv
import joblib
import warnings
warnings.filterwarnings('ignore')

# input: dataframe with pairs + QTs + Bayestar Skymaps + ML models.
# output: ML QTs predictions + ML Sky predictions + ML combined predictions
# process: Calc features/densenet predictions QTs--> XGboost QT  , Calc Features Skymaps --> XGboost Sky, Multiply.
# optional: Get False Positive Probabilities using distribution of ML
# statictic with background injections


def main():
    parser = argparse.ArgumentParser(
        description='This is stand alone code for computing ML predictions for a given event pairs in dataframe, their skymaps, Qtransforms and the trained ML models. Optionally computes the False Positive Probabilities given the background.')
    parser.add_argument(
        '-config',
        '--config',
        help='input CONFIG.yaml file',
        required=True)
    parser.add_argument('-event1',
        '--event1',
        help='input event1 name, required for single pair analysis',
        default='S0')
    parser.add_argument('-event2',
        '--event2',
        help='input event2 name, required for single pair analysis',
        default='S1')
    parser.add_argument('-singlepair','--singlepair', type=int, default=1)
    parser.add_argument('-outdir','--outdir', required = True, help = 'The output directory for the run')
    parser.add_argument('-label','--label', required = True, help = 'The label to use for the run')
    
    args = parser.parse_args()
      
    
    def set_var(var_name, value):
        globals()[var_name] = value
        
    def setvarnone(var):
        if var not in globals():
            globals()[var] = None
       
    stream = open(args.config, 'r')
    dictionary = yaml.full_load(stream)
    print(dictionary)
    #for doc in dictionary:
    for key, value in dictionary.items():
        print(key + " : " + str(value))
        set_var(key, value)
        
    output_directory = f'{args.outdir}/result'
    Path(output_directory).mkdir(parents = True, exist_ok = True)
    outfile = f'{output_directory}/alensid_{args.label}_result.json'
    
    if args.singlepair == 1:
        print('\n\n\n Running analysis for single pair: %s-%s \n\n'%(args.event1,args.event2))
        pair_df = pd.DataFrame()
        pair_df['img_0']=[args.event1]
        pair_df['img_1']=[args.event2]
        pair_df['Lensing']=''
        print(pair_df)
        
        in_df_pair = f'{output_directory}/alensid_{args.label}_pairs.csv'
    else:
        print('\n\n\n Running analysis for multiple pairs from Dataframe: %s \n\n'%in_df)
        in_df_pair=in_df
    
    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    
    
    if args.singlepair==1:
        pair_df.to_csv(in_df_pair)


    setvarnone('data_dir_sky_0')
    setvarnone('data_dir_sky')
    setvarnone('data_dir_qts')
    setvarnone('data_dir_sky_1')
    setvarnone('data_dir_qts_1')
    setvarnone('data_dir_qts_0')
    hlv_combs = ['h_stat','l_stat','v_stat','hl_stat','hv_stat','lv_stat','hlv_stat']
    
    if calc_features_sky == 1:
        print('Calculating Sky features...')
        #    _main(data_dir,start, n,infile,outfile,pe_skymaps)

        sky_ml_features._main(data_dir_sky, 0, 0 , in_df_pair, (outfile[:-5]+'_sky.csv'), 0,data_dir_sky_0,data_dir_sky_1)

    if cal_features_qts == 1:
        #_main(data_dir, n, infile, outfile, start, dense_models_dir, model_id, whitened)

        qt_df=in_df_pair
        for det in dets:
            for model_id in model_ids:
                print('Calculating QTs ML predictions for ', det)

                dense_model=dense_models_dict[det]
                df_qts=qts_ml_features._main(data_dir_qts,0,qt_df,(outfile[:-5]+'_qts.csv'),0, file_type, colored, det,dense_model, model_id, whitened,data_dir_qts_0,data_dir_qts_1)
                qt_df = outfile[:-5]+'_qts.csv'

    
    df_qts = pd.read_csv(outfile[:-5]+'_qts.csv')
    df_qts=ml.combinations_det_stats (df_qts,'dense_H1_'+str(model_id),'dense_L1_'+str(model_id),'dense_V1_'+str(model_id))
    df_qts['ML QTs'] = np.nanmean(df_qts[hlv_combs],axis=1)
    
    print('ML QTs:\n', df_qts.sort_values('ML QTs',ascending=False))
    df_qts.to_csv(outfile[:-5]+'_qts.csv')
    
    print('Calculating Sky ML predictions...')
    xgb_sky = joblib.load(xgboost_sky)
    df_sky = pd.read_csv(outfile[:-5]+'_sky.csv')
    df_sky = ml.XGB_predict(df_sky, xgb_sky)
    df_sky.to_csv(outfile[:-5]+'_sky.csv')
    print('ML Sky:\n', df_sky.sort_values('xgb_pred_bayestar_skymaps',ascending=False))

    print('Done. predictions saved at:', outfile[:-5]+'_sky.csv')
    
    
    print('Calculating combined ML predictions...')

    df_combined = pd.merge(df_qts, df_sky, on=["img_0", "img_1", "Lensing"]).reset_index(drop=True)
    for det_stat in hlv_combs:
        df_combined[det_stat+'_combined'] = df_combined[det_stat]*df_combined['xgb_pred_bayestar_skymaps']
        
    combined_cols = [comb +'_combined' for comb in hlv_combs]
    combined_cols.append('xgb_pred_bayestar_skymaps')
    df_combined['ML stat combined'] = np.nanmin(df_combined[combined_cols],axis=1)

    if calc_bc ==1:
        df_combined['SNR_0']=np.nan
        df_combined['ChirpMass_1']=np.nan
        df_combined['SNR_1']=np.nan
        df_combined['ChirpMass_0']=np.nan
        info_df_read = pd.read_csv(info_df,index_col=0)
        info_df_read['ChirpMass'] = conv.mchirp_from_mass1_mass2(info_df_read['Mass1'],info_df_read['Mass2'])


        for i in range(len(df_combined)):
            #try:
            mchirp0,snr0,mchirp1,snr1 = bc.get_mass_snr(df_combined['img_0'][i],df_combined['img_1'][i],info_df_read)
            df_combined['SNR_0'].iloc[i]=snr0
            df_combined['ChirpMass_1'].iloc[i]=mchirp0
            df_combined['SNR_1'].iloc[i]=snr1
            df_combined['ChirpMass_0'].iloc[i]=mchirp1
           # except:
           #     print(i, df_combined['img_0'][i],df_combined['img_1'][i])
            
        
        df_combined['bhattacharya dist']=np.nan
        df_combined['bhattacharya coeff formula']=np.nan
        df_combined['sigma_img0']=  (0.08*8/df_combined['SNR_0'])*df_combined['ChirpMass_0']
        df_combined['sigma_img1']=  (0.08*8/df_combined['SNR_1'])*df_combined['ChirpMass_1']
        df_combined['bhattacharya dist'] = bc.bhattacharya_dist_v(df_combined['ChirpMass_0'],df_combined['sigma_img0'],df_combined['ChirpMass_1'],df_combined['sigma_img1'])
        df_combined['bhattacharya coeff formula' ]= bc.bhattacharya_coeff_formula_v(df_combined['ChirpMass_0'],df_combined['sigma_img0'],df_combined['ChirpMass_1'],df_combined['sigma_img1'])


    if calc_fpp ==1:
        print('Calculating False postive probabilities (FPPs)...')
        in_background_df=pd.read_csv(background_df)
        in_background_df= ml.combinations_det_stats(in_background_df,'dense_H1_0','dense_L1_0','dense_V1_0')

        df_combined['xgb_pred_bayestar_skymaps_fpp']=ml.get_fars(df_combined,'xgb_pred_bayestar_skymaps',in_background_df,'xgb_pred_bayestar_skymaps')
        for det_stat in hlv_combs:
            in_background_df[det_stat+'_combined'] = in_background_df[det_stat]*in_background_df['xgb_pred_bayestar_skymaps']
            
            df_combined[det_stat[:-5]+'_fpp'] = ml.get_fars(df_combined,det_stat,in_background_df,det_stat)
            
            df_combined[det_stat[:-5]+'_combined_fpp'] = ml.get_fars(df_combined,det_stat+'_combined',in_background_df,det_stat+'_combined')
        
        
        combined_fpp_cols = [comb[:-5] +'_combined_fpp' for comb in hlv_combs]
        combined_fpp_cols.append('xgb_pred_bayestar_skymaps_fpp')
        df_combined['ML FPP combined'] = np.nanmean(df_combined[combined_fpp_cols],axis=1)
        df_combined['is_lensing_favored'] = 0
        ids=np.where(df_combined['ML FPP combined'].values<=fpp_threshold)[0]
        df_combined['is_lensing_favored'][ids]=1
        
        if calc_bc == 1: #veto
            ids=np.where(df_combined['bhattacharya dist'].values>=bd_threshold)[0]
            df_combined['is_lensing_favored'][ids]=0 
            
        df_combined.drop(columns=['Unnamed: 0_x','Unnamed: 0_y','Lensing'],inplace=True)

    print('ML combined: ', df_combined.sort_values('ML stat combined',ascending=False))
   
    df_combined_dict=df_combined.to_dict('records')
    #df_combined.to_json(outfile,orient="records", indent=4)
    with open(outfile, 'w') as fout:
        if args.singlepair ==1:
            json_dumps_str=json.dumps(df_combined_dict[0], indent = 4)
        else:
            json_dumps_str=json.dumps(df_combined_dict, indent = 4)
        print(json_dumps_str, file=fout)

    df_combined.to_csv(outfile[:-5]+'_combined.csv')



    print('ML predictions saved to dataframe %s and json %a' %(outfile[:-5]+'_combined.csv',outfile))




if __name__ == "__main__":
    main()


